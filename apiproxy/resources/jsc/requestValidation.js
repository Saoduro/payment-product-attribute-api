var billingId = context.getVariable("request.queryparam.billingId");
var mock = context.getVariable("request.queryparam.mock");
context.setVariable("isValidRequest", true);

print("context.getVariable(" +context.getVariable("request.queryparam.billingId"))
if(billingId === null) {
    context.setVariable("isValidRequest", false);
    context.setVariable("queryParam", "billingId");
    context.setVariable("errorMessage", "Please provide the valid billingId.");

} else if (mock !== null && context.getVariable("isValidRequest") === true) {
    if (mock != "success" && mock != "error" ) {
        context.setVariable("isValidRequest", false);
        context.setVariable("queryParam", "mock");
        context.setVariable("errorMessage", "Please provide the valid mock value.");
    }
}
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("mock", mock);
context.setVariable("billingId", billingId);
context.setVariable("correlationId", context.getVariable("request.queryparam.correlationId"));