var billingId = context.getVariable("request.queryparam.billingId");
//create comma seprated billingId string

// OPA request
var headers = {};
headers.authorization = context.getVariable("request.header.authorization");

var http = {};
http.headers = headers;
http.method = context.getVariable("request.verb");

http.keyMappingsUrl = "http://"+context.getVariable("private.keymapping")+"/api/v2/customers/"+billingId+"/keylookup?idtype=BILLING_ID";

var request = {};
request.http = http;

var attributes = {};
attributes.request = request;

var input = {};
input.attributes = attributes;

var opa_request = {};
opa_request.input = input

context.setVariable("opa_request", JSON.stringify(opa_request));
context.setVariable("data.api.path","/system/authorize_billing_id");