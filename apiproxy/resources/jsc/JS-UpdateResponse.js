var response = JSON.parse(context.getVariable("response.content"));
var availablePaymentDates = response.paymentProduct.availablePaymentDates;

if(availablePaymentDates === "") {
    response.paymentProduct.availablePaymentDates = [];
} else if(typeof availablePaymentDates === 'string' && availablePaymentDates.length > 0) {
    var availablePaymentDate = [];
    availablePaymentDate.push(availablePaymentDates);
    response.paymentProduct.availablePaymentDates = availablePaymentDate;
} else {
    response.paymentProduct.availablePaymentDates = availablePaymentDates;
}

context.setVariable("response.content",  JSON.stringify(response));