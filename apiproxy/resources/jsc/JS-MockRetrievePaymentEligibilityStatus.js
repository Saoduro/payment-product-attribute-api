var billingId = context.getVariable("request.queryparam.billingId");
var correlationId = context.getVariable("correlationId");
var mock = context.getVariable("request.queryparam.mock");
var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

var mockResponse = {};
var paymentProduct = {};
var availablePaymentDates = [];

mockResponse.correlationId = correlationId;

if (mock == "success" || mock === null) {
    paymentProduct.billingId = billingId;
    paymentProduct.eligible = "true";
    paymentProduct.minimumPayment = "1.00";
    paymentProduct.maximumPayment = "1000.00";
    availablePaymentDates.push("2020-07-04");
    availablePaymentDates.push("2020-09-05");
    paymentProduct.availablePaymentDates = availablePaymentDates;
    mockResponse.paymentProduct = paymentProduct;
} else {
    paymentProduct.billingId = billingId;
    paymentProduct.eligible = "false";
    paymentProduct.ineligibleReason = "Account password is incorrect.";
    mockResponse.paymentProduct = paymentProduct;
}

context.setVariable("response.content", JSON.stringify(mockResponse));
context.setVariable('response.header.Content-Type', "application/json");
